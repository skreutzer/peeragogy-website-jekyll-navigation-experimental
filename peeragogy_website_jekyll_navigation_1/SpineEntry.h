/**
 * @file $/SpineEntry.h
 * @author Stephan Kreutzer
 * @since 2020-03-02
 */

#ifndef _SPINEENTRY_H
#define _SPINEENTRY_H


#include <string>


class SpineEntry
{
public:
    SpineEntry();

public:
    const std::string& GetPage();
    int SetPage(const std::string& strPage);

    const std::string& GetCaption();
    int SetCaption(const std::string& strCaption);

    bool GetTopLevel();
    int SetTopLevel(bool bIsTopLevel);

    const std::string& GetFeaturedImageFileName();
    int SetFeaturedImageFileName(const std::string& strFeaturedImageFileName);

protected:
    std::string m_strPage;
    std::string m_strCaption;
    bool m_bTopLevel = true;
    std::string m_strFeaturedImageFileName;

};


#endif
