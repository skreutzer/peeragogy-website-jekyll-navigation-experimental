/**
 * @file $/peeragogy_website_jekyll_navigation_1.cpp
 * @author Stephan Kreutzer
 * @since 2020-03-02
 */


#include "SpineEntry.h"
#include "./cppstac/CSVInputFactory.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <memory>

typedef std::unique_ptr<cppstac::CSVInputFactory> CSVInputFactoryPtr;
typedef std::unique_ptr<cppstac::CSVEventReader> CSVEventReaderPtr;
typedef std::unique_ptr<cppstac::CSVEvent> CSVEventPtr;



#define INPUT_SPINE_FIELD_PAGE 1
#define INPUT_SPINE_FIELD_CAPTION 2
#define INPUT_SPINE_FIELD_ISTOPLEVEL 3
#define INPUT_SPINE_FIELD_FEATUREDIMAGEFILENAME 4



/** @todo If requested, turn this into a generic spine generator, maybe using NCX/OPF, as this is
  * just an ugly hack that would need to be replaced with a semantic XML source that generates the
  * spine below via XSLT or another templating engine. */
/** @todo If requested, port to another programming language. */
/** @todo If requested, write to an output file with a path specified by args,
  * so no terminal output redirect is needed any more. */
int main(int argc, char* argv[])
{
    /** @todo If requested, add a dedicated GUI for managing the spine, and maybe an importer/converter
      * from the grep index.html spine. */
    /** @todo If requested, expand SpineEntry to a nested structure. */
    // strFeaturedImageFileName is without effect if bTopLevel == false, but
    // keep it in case it eventually gets used or changed one day.
    std::vector<SpineEntry> aSpineEntries;

    {
        std::string strInputFilePath = "./spine.csv";

        if (argc >= 2)
        {
            strInputFilePath = argv[1];
        }

        std::unique_ptr<std::ifstream> pStream = std::unique_ptr<std::ifstream>(new std::ifstream);
        pStream->open(strInputFilePath, std::ios::in | std::ios::binary);

        if (pStream->is_open() != true)
        {
            std::cerr << "Couldn't open input file '" << strInputFilePath << "'." << std::endl;
            return 1;
        }

        CSVInputFactoryPtr pFactory = cppstac::CSVInputFactory::newInstance();
        pFactory->setIgnoreFirstLine(true);

        CSVEventReaderPtr pReader = pFactory->createCSVEventReader(*pStream);

        std::unique_ptr<SpineEntry> pSpineEntry = nullptr;

        while (pReader->hasNext() == true)
        {
            CSVEventPtr pEvent = pReader->nextEvent();

            if (pEvent->isStartLine() == true)
            {
                if (pSpineEntry != nullptr)
                {
                    std::cerr << "CSV Reader entering a new line while still processing the previous one!" << std::endl;
                    pStream->close();
                    return 1;
                }

                pSpineEntry = std::unique_ptr<SpineEntry>(new SpineEntry());
            }
            else if (pEvent->isField() == true)
            {
                if (pSpineEntry == nullptr)
                {
                    std::cerr << "CSV Reader encountered a field without entering a line first!" << std::endl;
                    pStream->close();
                    return 1;
                }

                cppstac::Field aField = pEvent->asField();

                /**
                 * @todo There's no check if the fields get all read/set, but
                 *     they should be in consecutive order anyway (not using
                 *     first line column captions here) and the CSV Streaming
                 *     API implementation too makes sure that the source is a
                 *     regular table, so it remains open if checks need to be
                 *     performed (counting, bit flags, reading the fields
                 *     consecutively in linear code not using the loop
                 *     iteration, other).
                 */
                switch (aField.GetFieldNumber())
                {
                case INPUT_SPINE_FIELD_PAGE:
                    pSpineEntry->SetPage(aField.GetData());
                    break;
                case INPUT_SPINE_FIELD_CAPTION:
                    pSpineEntry->SetCaption(aField.GetData());
                    break;
                case INPUT_SPINE_FIELD_ISTOPLEVEL:
                    if (aField.GetData().empty() == true ||
                        aField.GetData() == "false")
                    {
                        pSpineEntry->SetTopLevel(false);
                    }
                    else
                    {
                        pSpineEntry->SetTopLevel(true);
                    }

                    break;
                case INPUT_SPINE_FIELD_FEATUREDIMAGEFILENAME:
                    pSpineEntry->SetFeaturedImageFileName(aField.GetData());
                    break;
                default:
                    std::cerr << "Unknown CSV input spine field " << aField.GetFieldNumber() << std::endl;
                    pStream->close();
                    return 1;
                }
            }
            else if (pEvent->isEndLine() == true)
            {
                if (pSpineEntry == nullptr)
                {
                    std::cerr << "CSV Reader encountered the end of a line before it was properly started!" << std::endl;
                    pStream->close();
                    return 1;
                }

                aSpineEntries.push_back(*pSpineEntry);
                pSpineEntry = nullptr;
            }
            else
            {

            }
        }

        pStream->close();
    }

    std::cout << "      <!-- Custom headers. -->\n"
              << "      {% comment %}\n"
              << "      This list was automatically generated by peeragogy_website_jekyll_navigation_1 (see https://gitlab.com/skreutzer/peeragogy-website-jekyll-navigation-experimental and https://publishing-systems.org).\n"
              << "      {% endcomment %}\n";

    for (std::vector<SpineEntry>::iterator iter = aSpineEntries.begin();
         iter != aSpineEntries.end();
         iter++)
    {
        if (iter == aSpineEntries.begin())
        {
            std::cout << "      {% if page.url == \"" << iter->GetPage() << "\" %}\n";
        }
        else
        {
            if (iter->GetTopLevel() == true)
            {
                std::cout << "      {% elsif page.url == \"" << iter->GetPage() << "\" %}\n";
            }
        }

        if (iter->GetTopLevel() == true)
        {
            if (iter->GetFeaturedImageFileName().empty() != true)
            {
                // Assumption is that the entries in aSpineEntries don't inject path exploitations here.
                std::cout << "      <div class=\"featured-image\" style=\"background-image: url(../images/featured/" << iter->GetFeaturedImageFileName()  << ")\"></div>\n";
            }
            else
            {
                std::cout << "      <!-- No featured image configured. Would need to be added to peeragogy_website_jekyll_navigation_1 (see https://gitlab.com/skreutzer/peeragogy-website-jekyll-navigation-experimental and https://publishing-systems.org) to automatically generate the corresponding part in the $/_layouts/default.html Jekyll template (see https://github.com/Peeragogy/Peeragogy.github.io/blob/master/_layouts/default.html and http://peeragogy.org). -->\n";
            }
        }
    }

    std::cout << "      {% else %}\n"
              << "      <!-- This page is not configured in the generator! Please add the entry to peeragogy_website_jekyll_navigation_1 (see https://gitlab.com/skreutzer/peeragogy-website-jekyll-navigation-experimental and https://publishing-systems.org) and update the $/_layouts/default.html Jekyll template (see https://github.com/Peeragogy/Peeragogy.github.io/blob/master/_layouts/default.html and http://peeragogy.org) accordingly. -->\n"
              << "      {% endif %}\n"
              << std::endl;

    std::cout << "      {% comment %}\n"
              << "      This list was automatically generated by peeragogy_website_jekyll_navigation_1 (see https://gitlab.com/skreutzer/peeragogy-website-jekyll-navigation-experimental and https://publishing-systems.org).\n"
              << "      {% endcomment %}\n";

    for (std::vector<SpineEntry>::iterator iterCurrent = aSpineEntries.begin(), iterPrevious = aSpineEntries.end();
         iterCurrent != aSpineEntries.end();
         iterPrevious = iterCurrent, iterCurrent++)
    {
        if (iterCurrent == aSpineEntries.begin())
        {
            std::cout << "      {% if page.url == \"" << iterCurrent->GetPage() << "\"%}\n"
                      << "      Next: <a href=\"." << (iterCurrent + 1)->GetPage() << "\">" << (iterCurrent + 1)->GetCaption() << " »</a>\n";
            continue;
        }

        std::cout << "      {% elsif page.url == \"" << iterCurrent->GetPage() << "\" %}\n"
                  << "      ";

        if (iterPrevious != aSpineEntries.end())
        {
            std::cout << "Previous: <a href=\"." << iterPrevious->GetPage() << "\">« " << iterPrevious->GetCaption() << "</a>";
        }

        if ((iterCurrent + 1) != aSpineEntries.end())
        {
            std::cout << "  &nbsp; Next: <a href=\"." << (iterCurrent + 1)->GetPage() << "\">" << (iterCurrent + 1)->GetCaption() << " »</a>";
        }

        std::cout << "\n";
    }

    std::cout << "      {% else %}\n"
              << "      <!-- This page is not configured in the generator! Please add the entry to peeragogy_website_jekyll_navigation_1 (see https://gitlab.com/skreutzer/peeragogy-website-jekyll-navigation-experimental and https://publishing-systems.org) and update the $/_layouts/default.html Jekyll template (see https://github.com/Peeragogy/Peeragogy.github.io/blob/master/_layouts/default.html and http://peeragogy.org) accordingly. -->\n"
              << "      {% endif %}\n"
              << std::endl;

    return 0;
}
