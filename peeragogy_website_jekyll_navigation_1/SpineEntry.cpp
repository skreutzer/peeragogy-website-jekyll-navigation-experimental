/**
 * @file $/SpineEntry.cpp
 * @author Stephan Kreutzer
 * @since 2020-03-02
 */


#include "SpineEntry.h"



SpineEntry::SpineEntry()
{

}

const std::string& SpineEntry::GetPage()
{
    return m_strPage;
}

int SpineEntry::SetPage(const std::string& strPage)
{
    m_strPage = strPage;
    return 0;
}

const std::string& SpineEntry::GetCaption()
{
    return m_strCaption;
}

int SpineEntry::SetCaption(const std::string& strCaption)
{
    m_strCaption = strCaption;
    return 0;
}

bool SpineEntry::GetTopLevel()
{
    return m_bTopLevel;
}

int SpineEntry::SetTopLevel(bool bIsTopLevel)
{
    m_bTopLevel = bIsTopLevel;
    return 0;
}

const std::string& SpineEntry::GetFeaturedImageFileName()
{
    return m_strFeaturedImageFileName;
}

int SpineEntry::SetFeaturedImageFileName(const std::string& strFeaturedImageFileName)
{
    m_strFeaturedImageFileName = strFeaturedImageFileName;
    return 0;
}
