#!/bin/sh
sudo apt-get install wget unzip make g++

cd ./peeragogy_website_jekyll_navigation_1/
wget https://gitlab.com/publishing-systems/CppStAC/-/archive/master/CppStAC-master.zip
unzip ./CppStAC-master.zip
rm ./CppStAC-master.zip
mv ./CppStAC-master/ ./cppstac/
make
cd ..
